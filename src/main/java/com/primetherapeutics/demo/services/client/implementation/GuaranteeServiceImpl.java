package com.primetherapeutics.demo.services.client.implementation;

import com.primetherapeutics.demo.domain.*;
import com.primetherapeutics.demo.services.client.GuaranteeService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class GuaranteeServiceImpl implements GuaranteeService {

    //    For reference, but not in use:
    //    GuaranteeRepository guaranteeRepository;
    //
    //    public GuaranteeServiceImpl(GuaranteeRepository guaranteeRepository) {
    //        this.guaranteeRepository = guaranteeRepository;
    //    }


    @Override
    public List<Guarantee> findAll() {
        // For reference, but not in use:
        // return guaranteeRepository.findAll();

        List<Guarantee> list = new ArrayList<>();

        GuaranteeProfile guaranteeProfile1 = new GuaranteeProfile("Adient16", GuaranteeBasis.REBATE_PLUS_ADMIN_DOLLARS, ClaimBasis.BRANDED, GuaranteeCalcMethod.SCRIPTS, false, LocalDate.parse(new StringBuffer("2016-07-01")), LocalDate.parse(new StringBuffer("2016-12-31")));
        GuaranteeProfile guaranteeProfile2 = new GuaranteeProfile("Adient17", GuaranteeBasis.REBATE_PLUS_ADMIN_DOLLARS, ClaimBasis.BRANDED, GuaranteeCalcMethod.SCRIPTS, false, LocalDate.parse(new StringBuffer("2017-01-01")), LocalDate.parse(new StringBuffer("2018-12-31")));

        Guarantee guarantee1 = new Guarantee("Adient16", "GTE00000177", new Date("07/01/2016"), new Date("12/31/2016"), guaranteeProfile1);
        Guarantee guarantee2 = new Guarantee("Adient17", "GTE00000226", new Date("01/01/2017"), new Date("12/31/2018"), guaranteeProfile2);

        list.add(guarantee1);
        list.add(guarantee2);

        return list;
    }
}
