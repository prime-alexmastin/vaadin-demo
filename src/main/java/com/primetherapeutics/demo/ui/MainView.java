package com.primetherapeutics.demo.ui;

import com.primetherapeutics.demo.domain.*;
import com.primetherapeutics.demo.services.client.GuaranteeService;
import com.primetherapeutics.demo.services.client.implementation.GuaranteeServiceImpl;
import com.vaadin.flow.component.accordion.Accordion;
import com.vaadin.flow.component.accordion.AccordionPanel;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.router.Route;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Route
public class MainView extends VerticalLayout {

    private Guarantee selectedGuarantee;
    private GuaranteeProfile selectedGuaranteeProfile;

    private TextField guaranteeName;
    private RadioButtonGroup<GuaranteeBasis> guaranteeBasis;
    private RadioButtonGroup<ClaimBasis> claimBasis;
    private RadioButtonGroup<GuaranteeCalcMethod> guaranteeCalcMethod;
    private Checkbox vaccine;
    private DatePicker startDatePicker;
    private DatePicker endDatePicker;

    private Accordion accordion;
    private Grid guaranteeProfileGrid;

    public MainView() {
        accordion = initAccordion();
        accordion.add(createGuaranteePanel());
        accordion.add(createGuaranteeProfilePanel());

        add(accordion);
    }

    private Accordion initAccordion() {
        Accordion accordion = new Accordion();
        accordion.setWidthFull();

        return accordion;
    }

    private Grid getGuaranteeGrid() {
        GuaranteeService guaranteeService = new GuaranteeServiceImpl();
        Grid guaranteeGrid = new Grid<>(Guarantee.class);
        ListDataProvider<Guarantee> dataProvider = new ListDataProvider<>(guaranteeService.findAll());

        guaranteeGrid.setDataProvider(dataProvider);
        guaranteeGrid.setColumns("name", "id", "startDate", "endDate");
        guaranteeGrid.setWidthFull();

        guaranteeGrid.asSingleSelect().addValueChangeListener(event -> {
            selectedGuarantee = (Guarantee) event.getValue();
            GuaranteeProfile guaranteeProfile = selectedGuarantee.getGuaranteeProfile();

            guaranteeProfileGrid.setItems(guaranteeProfile);
            guaranteeProfileGrid.getDataProvider().refreshAll();
            accordion.open(1);
        });

        return guaranteeGrid;
    }

    private Grid getGuaranteeProfileGrid() {
        guaranteeProfileGrid = new Grid<>(GuaranteeProfile.class);
        GuaranteeProfile guaranteeProfile = (selectedGuarantee != null) ? selectedGuarantee.getGuaranteeProfile() : new GuaranteeProfile();

        List<GuaranteeProfile> list = new ArrayList<>();
        list.add(guaranteeProfile);

        ListDataProvider<GuaranteeProfile> dataProvider = new ListDataProvider<>(list);
        guaranteeProfileGrid.setDataProvider(dataProvider);
        guaranteeProfileGrid.setColumns("name", "guaranteeBasis", "startDate", "endDate");
        guaranteeProfileGrid.setWidthFull();

        guaranteeProfileGrid.asSingleSelect().addValueChangeListener(event -> {
            selectedGuaranteeProfile = (GuaranteeProfile) event.getValue();
            if (selectedGuaranteeProfile != null) populateGuaranteeProfileForm();
            else clearGuaranteeProfileForm();
        });

        return guaranteeProfileGrid;
    }

    private Button getSaveButton() {
        Button saveButton = createRightAlignedButton("Save");
        saveButton.addClickListener(event -> {
            System.out.println("Save");
            // mgr.persist(entity);
        });
        return saveButton;
    }

    private Button getClearButton() {
        Button clearButton = createRightAlignedButton("Clear");
        clearButton.addClickListener(event -> {
            clearGuaranteeProfileForm();
        });
        return clearButton;
    }

    private Button getDeleteButton() {
        Button deleteButton = createRightAlignedButton("Delete");
        deleteButton.addClickListener(event -> {
            System.out.println("Delete");
            // mgr.delete(entity); Not sure if exists?
        });
        return deleteButton;
    }

    private Button createRightAlignedButton(String text) {
        Button button = new Button(text);
        button.getElement().getStyle().set("float", "right");
        button.getElement().getStyle().set("margin", "5px");

        return button;
    }

    private AccordionPanel createGuaranteePanel() {
        AccordionPanel guaranteePanel = new AccordionPanel();
        guaranteePanel.setSummaryText("Guarantee");
        guaranteePanel.addContent(getGuaranteeGrid());
        guaranteePanel.addContent(getSaveButton());
        guaranteePanel.addContent(getDeleteButton());

        return guaranteePanel;
    }

    private AccordionPanel createGuaranteeProfilePanel() {
        AccordionPanel guaranteeProfilePanel = new AccordionPanel();
        guaranteeProfilePanel.setSummaryText("Guarantee Profile");
        guaranteeProfilePanel.addContent(getGuaranteeProfileGrid());
        guaranteeProfilePanel.addContent(getGuaranteeProfileForm());
        guaranteeProfilePanel.addContent(getSaveButton());
        guaranteeProfilePanel.addContent(getClearButton());

        return guaranteeProfilePanel;
    }

    private VerticalLayout getGuaranteeProfileForm() {
        VerticalLayout verticalLayout = new VerticalLayout();
        Binder<GuaranteeProfile> binder = new Binder<>();

        FormLayout guaranteeNameRow = new FormLayout();
        guaranteeName = new TextField("Guarantee Name");
        guaranteeName.setRequired(true);
        guaranteeName.setRequiredIndicatorVisible(true);
        guaranteeNameRow.add(guaranteeName, 1);

        FormLayout guaranteeBasisRow = new FormLayout();
        Span guaranteeBasisLabel = new Span("Guarantee Basis");
        guaranteeBasisLabel.getStyle().set("align-self", "center");
        guaranteeBasis = new RadioButtonGroup<>();
        guaranteeBasis.setItems(GuaranteeBasis.values());
        guaranteeBasis.setRequired(true);
        guaranteeBasis.setRequiredIndicatorVisible(true);
        guaranteeBasisRow.add(guaranteeBasisLabel, guaranteeBasis);

        FormLayout claimBasisRow = new FormLayout();
        Span claimBasisLabel = new Span("Claim Basis");
        claimBasisLabel.getStyle().set("align-self", "center");
        claimBasis = new RadioButtonGroup<>();
        claimBasis.setItems(ClaimBasis.values());
        claimBasis.setRequired(true);
        claimBasis.setRequiredIndicatorVisible(true);
        claimBasisRow.add(claimBasisLabel, claimBasis);

        FormLayout guaranteeCalcMethodRow = new FormLayout();
        Span guaranteeCalcMethodLabel = new Span("Guarantee Calc Method");
        guaranteeCalcMethodLabel.getStyle().set("align-self", "center");
        guaranteeCalcMethod = new RadioButtonGroup<>();
        guaranteeCalcMethod.setItems(GuaranteeCalcMethod.values());
        guaranteeCalcMethod.setRequired(true);
        guaranteeCalcMethod.setRequiredIndicatorVisible(true);
        guaranteeCalcMethodRow.add(guaranteeCalcMethodLabel, guaranteeCalcMethod);

        FormLayout excludeClaimsRow = new FormLayout();
        Span excludeClaimsLabel = new Span("Exclude Claims");
        excludeClaimsLabel.getStyle().set("align-self", "center");
        excludeClaimsLabel.getStyle().set("align-self", "center");
        vaccine = new Checkbox("Vaccine");
        excludeClaimsRow.add(excludeClaimsLabel, vaccine);

        FormLayout dateRow = new FormLayout();
        Span startDateLabel = new Span("Start Date");
        startDateLabel.getStyle().set("align-self", "center");
        startDatePicker = new DatePicker();
        startDatePicker.setRequired(true);
        startDatePicker.setRequiredIndicatorVisible(true);
        Span endDateLabel = new Span("End Date");
        endDateLabel.getStyle().set("align-self", "center");
        endDatePicker = new DatePicker();
        endDatePicker.setRequired(true);
        endDatePicker.setRequiredIndicatorVisible(true);
        dateRow.add(startDateLabel, startDatePicker, endDateLabel, endDatePicker);

        Binder.Binding<GuaranteeProfile, String> guaranteeNameBinding = binder.forField(guaranteeName)
                .bind(GuaranteeProfile::getName, GuaranteeProfile::setName);
        guaranteeName.addValueChangeListener(event -> {
            if (!guaranteeNameBinding.validate().isError() && selectedGuaranteeProfile != null) {
                selectedGuaranteeProfile.setName(event.getValue());
            }
        });

        Binder.Binding<GuaranteeProfile, GuaranteeBasis> guaranteeBasisBinding = binder.forField(guaranteeBasis)
                .bind(GuaranteeProfile::getGuaranteeBasis, GuaranteeProfile::setGuaranteeBasis);
        guaranteeBasis.addValueChangeListener(event -> {
            if (!guaranteeBasisBinding.validate().isError() && selectedGuaranteeProfile != null) {
                selectedGuaranteeProfile.setGuaranteeBasis(event.getValue());
            }
        });

        Binder.Binding<GuaranteeProfile, ClaimBasis> claimBasisBinding = binder.forField(claimBasis)
                .bind(GuaranteeProfile::getClaimBasis, GuaranteeProfile::setClaimBasis);
        claimBasis.addValueChangeListener(event -> {
            if (!claimBasisBinding.validate().isError() && selectedGuaranteeProfile != null) {
                selectedGuaranteeProfile.setClaimBasis(event.getValue());
            }
        });

        Binder.Binding<GuaranteeProfile, GuaranteeCalcMethod> guaranteeCalcMethodBinding = binder.forField(guaranteeCalcMethod)
                .bind(GuaranteeProfile::getGuaranteeCalcMethod, GuaranteeProfile::setGuaranteeCalcMethod);
        guaranteeCalcMethod.addValueChangeListener(event -> {
            if (!guaranteeCalcMethodBinding.validate().isError() && selectedGuaranteeProfile != null) {
                selectedGuaranteeProfile.setGuaranteeCalcMethod(event.getValue());
            }
        });

        Binder.Binding<GuaranteeProfile, Boolean> vaccineBinding = binder.forField(vaccine)
                .bind(GuaranteeProfile::getExcludeClaims, GuaranteeProfile::setExcludeClaims);
        vaccine.addValueChangeListener(event -> {
            if (!vaccineBinding.validate().isError() && selectedGuaranteeProfile != null) {
                selectedGuaranteeProfile.setExcludeClaims(event.getValue());
            }
        });

        verticalLayout.add(guaranteeNameRow, guaranteeBasisRow, claimBasisRow, guaranteeCalcMethodRow, excludeClaimsRow, dateRow);

        return verticalLayout;
    }

    private void populateGuaranteeProfileForm() {
        guaranteeName.setValue(selectedGuaranteeProfile.getName());
        guaranteeBasis.setValue(selectedGuaranteeProfile.getGuaranteeBasis());
        claimBasis.setValue(selectedGuaranteeProfile.getClaimBasis());
        guaranteeCalcMethod.setValue(selectedGuaranteeProfile.getGuaranteeCalcMethod());
        vaccine.setValue(selectedGuaranteeProfile.getExcludeClaims());
        startDatePicker.setValue(selectedGuaranteeProfile.getStartDate());
        endDatePicker.setValue(selectedGuaranteeProfile.getEndDate());
    }

    private void clearGuaranteeProfileForm() {
        guaranteeName.clear();
        guaranteeBasis.clear();
        claimBasis.clear();
        guaranteeCalcMethod.clear();
        vaccine.clear();
        startDatePicker.clear();
        startDatePicker.setInvalid(false);
        endDatePicker.clear();
        endDatePicker.setInvalid(false);
    }
}
