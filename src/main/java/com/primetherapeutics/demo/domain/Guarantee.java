package com.primetherapeutics.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Guarantee {
    private String name;
    private String id;
    private Date startDate;
    private Date endDate;
    private GuaranteeProfile guaranteeProfile;
}
